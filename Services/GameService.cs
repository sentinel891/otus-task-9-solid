﻿using _9_SOLID_GuessGame.Interfaces;
using _9_SOLID_GuessGame.Models;
using _9_SOLID_GuessGame.Options;

namespace _9_SOLID_GuessGame.Services
{
    class GameService: IGameService
    {
        public bool OutOfTries { get; set; }
        
        private DataService _dataService;
        private GameSettings _settings;
        private int _usedTries;

        public GameService(Settings settings)
        {
            _usedTries = 0;
            _dataService = new DataService(settings.InitializingSettings);
            _settings = settings.GameSettings;

            UpdateOutOfTries();
        }

        public CheckResult CheckWithGuess(int userNumber)
        {
            if (OutOfTries) return new CheckResult(0, OutOfTries);

            _usedTries += 1;
            UpdateOutOfTries();
            return new CheckResult(_dataService.CheckWithGuess(userNumber), OutOfTries);            
        }

        private void UpdateOutOfTries()
        {
            OutOfTries = (_settings.Tries <= _usedTries);
        }
    }
}
