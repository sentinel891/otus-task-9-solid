﻿using _9_SOLID_GuessGame.Options;
using System;

namespace _9_SOLID_GuessGame.Services
{
    class RandomService
    {
        private InitializingSettings _settings;
        public RandomService(InitializingSettings settings)
        {
            _settings = settings;
        }

        public int GenerateNumber()
        {
            Random rnd = new Random();
            return rnd.Next(_settings.MinNumber, _settings.MaxNumber);
        }
    }
}
