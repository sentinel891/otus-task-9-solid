﻿using _9_SOLID_GuessGame.Options;

namespace _9_SOLID_GuessGame.Services
{
    class DataService
    {
        private int _guessNumber;

        public DataService(InitializingSettings settings)
        {
            RandomService _randomGenerator = new RandomService(settings);
            _guessNumber = _randomGenerator.GenerateNumber();
        }

        public int CheckWithGuess (int userNumber)
        {
            if (userNumber == _guessNumber) return 0;  
            else if (userNumber > _guessNumber) return -1;
            else return 1;
        }
    }
}
