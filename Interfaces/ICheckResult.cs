﻿namespace _9_SOLID_GuessGame.Interfaces
{
    interface ICheckResult
    {
        public int Result { get; init; }
        public string Description { get; init; }
        public bool OutOfTries { get; init; }
    }
}
