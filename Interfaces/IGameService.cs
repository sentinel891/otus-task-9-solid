﻿using _9_SOLID_GuessGame.Models;

namespace _9_SOLID_GuessGame.Interfaces
{
    public interface IGameService
    {
        public bool OutOfTries { get; set; }
        public CheckResult CheckWithGuess(int userNumber);
    }
}
