﻿using _9_SOLID_GuessGame.Interfaces;

namespace _9_SOLID_GuessGame.Models
{
    public class CheckResult: ICheckResult
    {
        public int Result { get; init; }
        public string Description { get; init; }
        public bool OutOfTries { get; init; }

        public CheckResult(int result, bool outOfTries)
        {
            Result = result;
            OutOfTries = outOfTries;

            //if (OutOfTries) Description = "У вас закончились попытки";
            //else
            switch (result)
            {
                case 0:
                    Description = "Загаданное число равно указанному";
                    break;
                case 1:
                    Description = "Загаданное число больше указанного";
                    break;
                case -1:
                    Description = "Загаданное число меньше указанного";
                    break;
                default:
                    Description = "";
                    break;
            }
        }
    }
}
