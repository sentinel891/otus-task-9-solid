﻿namespace _9_SOLID_GuessGame.Options
{
    class GameSettings
    {
        public int Tries { get; set; }

        public GameSettings(int tries)
        {
            Tries = tries;
        }
    }
}
