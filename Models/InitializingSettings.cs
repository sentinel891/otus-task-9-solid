﻿namespace _9_SOLID_GuessGame.Options
{
    class InitializingSettings
    {
        public int MinNumber { get; set; }
        public int MaxNumber { get; set; }

        public InitializingSettings(int minNumber, int maxNumber)
        {
            MinNumber = minNumber;
            MaxNumber = maxNumber;
        }
    }
}
