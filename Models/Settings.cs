﻿namespace _9_SOLID_GuessGame.Options
{
    class Settings
    {
        public InitializingSettings InitializingSettings { get; set; }
        public GameSettings GameSettings { get; set; }

        public Settings(int minNumber, int maxNumber, int tries)
        {
            GameSettings = new GameSettings(tries);
            InitializingSettings = new InitializingSettings(minNumber, maxNumber);
        }
    }
}
