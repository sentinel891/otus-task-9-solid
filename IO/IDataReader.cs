﻿namespace SOLID.IO
{
    public interface IDataReader
    {
        public int Read();
    }
}
