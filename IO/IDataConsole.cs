﻿namespace SOLID.IO
{
    public interface IDataConsole : IDataReader, IDataWriter
    {
    }
}
