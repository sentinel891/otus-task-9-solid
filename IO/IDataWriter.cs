﻿namespace SOLID.IO
{
    public interface IDataWriter
    {
        public void Write(string text);

        public void Write(decimal number);
    }
}
