﻿namespace SOLID.IO
{
    using System;

    public class DataConsole : IDataConsole
    {
        public int Read() => int.Parse(s: Console.ReadLine());

        public void Write(string text) => Console.WriteLine(value: text);

        public void Write(decimal number) => Console.WriteLine(value: number);
    }
}
