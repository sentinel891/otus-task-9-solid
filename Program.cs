﻿using SOLID.IO;

namespace _9_SOLID_GuessGame
{
    class Program
    {
        static void Main(string[] args)
        {
            DataConsole dataWriter = new DataConsole();
            DataConsole dataReader = new DataConsole();

            Controller controller = new Controller(dataWriter, dataReader);
            controller.Execute();
        }
    }
}
