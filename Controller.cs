﻿using _9_SOLID_GuessGame.Interfaces;
using _9_SOLID_GuessGame.Models;
using _9_SOLID_GuessGame.Options;
using _9_SOLID_GuessGame.Services;
using SOLID.IO;

namespace _9_SOLID_GuessGame
{
    class Controller
    {
        private readonly IDataWriter _dataWriter;
        private readonly IDataReader _dataReader;
        private IGameService _gameService;

        public Controller(IDataWriter dataWriter, IDataReader dataReader)
        {
            this._dataWriter = dataWriter;
            this._dataReader = dataReader;
        }

        public void Execute()
        {
            Settings settings = this.ReadSettings();
            this.GuessGame(settings);
        }

        public Settings ReadSettings()
        {
            this._dataWriter.Write(text: "Введите настройки:");

            this._dataWriter.Write(text: "Минимум диапазона:");
            int minValue = this._dataReader.Read();

            this._dataWriter.Write(text: "Максимум диапазона:");
            int maxValue = this._dataReader.Read();

            this._dataWriter.Write(text: "Количество попыток:");
            int tries = this._dataReader.Read();

            return new Settings(minValue, maxValue, tries);
        }

        public void GuessGame(Settings settings)
        {
            _gameService = new GameService(settings);
            CheckResult checkResult = new CheckResult(-999, _gameService.OutOfTries);

            while (!checkResult.OutOfTries & checkResult.Result!=0) 
            {
                this._dataWriter.Write(text: "Введите значение:");
                int guessNumber = this._dataReader.Read();
                checkResult = _gameService.CheckWithGuess(guessNumber);
                this._dataWriter.Write(text: checkResult.Description);
            } 

            if (checkResult.Result == 0) this._dataWriter.Write(text: "Поздравляю с победой!");
            else this._dataWriter.Write(text: "У вас закончились попытки. В следующий раз повезет");
        }
    }
}
